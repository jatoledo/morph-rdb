# Morph-RDB

Morph-RDB (formerly called ODEMapster) is an RDB2RDF engine developed by the Ontology Engineering Group, that follows the R2RML specification (http://www.w3.org/TR/r2rml/).

Morph-RDB supports two operational modes: data upgrade (generating RDF instances from data in a relational database) and query translation (SPARQL to SQL). Morph-RDB employs various optimisation techniques in order to generate efficient SQL queries, such as self-join elimination and subquery elimination.

Morph-RDB has been tested with real queries from various Spanish/EU projects and has proven to work faster than other state-of-the-art tools available. At the moment, Morph-RDB works with MySQL, PostgreSQL, H2, CSV files and MonetDB.

User guides: For those who want to use this project on an user level, you can find a little guide to on the main branch wiki : https://github.com/oeg-upm/morph-rdb/wiki



**Morph-rdb folder contains**
---
- `db_data` - Contains `morph_example.sql` to be uploaded to the database
- `mappings` - Contains mapping file.
- `properties` - Contains propertie file.
- `queries` - Contain a sample query file.
- `results` - The results of the execution are saved here.
- `docker-compose.yml` - File for creating three docker containers: `morph-rdb`, `mysql_morph`, and `adminer`

**Installation**
---

1. Install
    + `$ git clone https://jatoledo@bitbucket.org/jatoledo/morph-rdb.git`
    + `$ cd morph-rdb`
    + `$ docker-compose up -d`

2. Check if the containers are started
    + `$ docker ps`
```bash
CONTAINER ID        IMAGE                                 COMMAND                  CREATED             STATUS              PORTS                   NAMES
f70f4b9b309f        oegdataintegration/morph-rdb:3.12.5   "tail -f /dev/null"      About an hour ago   Up 41 minutes       0.0.0.0:8000->80/tcp    morph-rdb
0891cd883950        mysql:5.7                             "docker-entrypoint.s…"   About an hour ago   Up 41 minutes       3306/tcp, 33060/tcp     mysql_morph
16190b32c9ae        adminer                               "entrypoint.sh docke…"   About an hour ago   Up 41 minutes       0.0.0.0:800->8080/tcp   adminer
```
**Usage**
---

- Step 1 : Open your browser `http://localhost:800`

![](images/img1.jpg)

- Step 2 : Upload data from the following path `db_data/morph_example.sql`
    + `Copy or upload  db_data/morph_example.sql`

![Load data Demo](images/morph-rdb.gif)

- Step 3 : Run moprh-rdb
     + `$ sh run.sh`

- Step 4 : Check your results in the results folder
    + `$ ls -l results`

**Configuration Options**
---



**How to Contribute**
---

1. Clone repo and create a new branch: `$ git checkout https://github.com/oeg-upm/morph-rdb -b name_for_new_branch`.
2. Make changes and test
3. Submit Pull Request with comprehensive description of changes

**Acknowledgements**
---

